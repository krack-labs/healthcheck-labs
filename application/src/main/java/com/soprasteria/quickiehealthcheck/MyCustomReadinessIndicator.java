package com.soprasteria.quickiehealthcheck;

import java.util.concurrent.atomic.AtomicBoolean;
import org.springframework.boot.actuate.availability.ReadinessStateHealthIndicator;
import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.AvailabilityState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.stereotype.Component;

@Component
public class MyCustomReadinessIndicator extends ReadinessStateHealthIndicator {

  public MyCustomReadinessIndicator(ApplicationAvailability availability) {
    super(availability);
  }

  @Override
  protected ReadinessState getState(ApplicationAvailability applicationAvailability) {
    return !ApiController.OUT
        ? ReadinessState.ACCEPTING_TRAFFIC
        : ReadinessState.REFUSING_TRAFFIC;
  }
}