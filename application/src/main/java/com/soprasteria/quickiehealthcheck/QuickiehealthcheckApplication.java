package com.soprasteria.quickiehealthcheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuickiehealthcheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuickiehealthcheckApplication.class, args);
	}

}
