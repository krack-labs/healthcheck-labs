package com.soprasteria.quickiehealthcheck;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.Optional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

	public static boolean OUT = false;
	public static boolean KILL = false;
	private ExecutorService executorService = Executors.newFixedThreadPool(10);



	@GetMapping("/")
	public String simple() {
        String podName = Optional.ofNullable(System.getenv("HOSTNAME")).orElse("rien");
		if(ApiController.KILL || ApiController.OUT ) throw new RuntimeException("pod "+podName+" is dead");
		return podName;
	}

	@DeleteMapping("/")
	public String delete(@RequestParam String id) {
		if("kill".equals(id)){
			ApiController.KILL = true;

		}else{
			ApiController.OUT = true;
		}
		this.executorService.submit(() -> {
			try{
				Thread.sleep(1*60*1000);
			}catch(InterruptedException e){

			}    

			ApiController.KILL = false;
			ApiController.OUT = false;
		});
        String podName = Optional.ofNullable(System.getenv("HOSTNAME")).orElse("rien");
		return podName;
	}
}