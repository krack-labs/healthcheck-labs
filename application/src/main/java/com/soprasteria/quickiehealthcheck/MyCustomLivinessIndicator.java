package com.soprasteria.quickiehealthcheck;

import java.util.concurrent.atomic.AtomicBoolean;
import org.springframework.boot.actuate.availability.LivenessStateHealthIndicator;
import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.AvailabilityState;
import org.springframework.boot.availability.LivenessState;
import org.springframework.stereotype.Component;

@Component
public class MyCustomLivinessIndicator extends LivenessStateHealthIndicator {

  public MyCustomLivinessIndicator(ApplicationAvailability availability) {
    super(availability);
  }

  @Override
  protected LivenessState getState(ApplicationAvailability applicationAvailability) {
    return !ApiController.KILL
        ? LivenessState.CORRECT
        : LivenessState.BROKEN;
  }
}