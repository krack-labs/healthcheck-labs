version=$1
cd application
docker login -u ${DOCKER_USER} -p ${DOCKER_PASSWORD}
docker build -t ${DOCKER_USER}/lab-metrics:$version .
docker push ${DOCKER_USER}/lab-metrics:$version

cd ..
sed -i "s\appVersion: \".*\"\appVersion: \"$version\"\g" helm/Chart.yaml

helm package helm/
helm upgrade -i  mypackage ./mypackage-0.1.0.tgz