## call service

```bash
while true; do .gitpod/ssh.sh curl -k -s https://localhost; echo ""; sleep 2s; done

```

## check liviness
```bash
.gitpod/ssh.sh curl -k -s https://localhost/actuator/health/liviness
```
## check readiness
```bash
.gitpod/ssh.sh curl -k -s https://localhost/actuator/health/readiness
```

# stop liviness 60s
```bash
.gitpod/ssh.sh curl -k -X DELETE https://localhost?id=out
```

# stop readyness 60s
```bash
.gitpod/ssh.sh curl -k -X DELETE https://localhost?id=out
```